#include "FreeRTOS.h"
#include "semphr.h"
#include "lwip_tls.h"
#include "mqtt-client.h"
#include <string.h>

static void __mbedtls_test(void * pv);
static void __mqtt_test_init(void);

void lan_init()
{
	tcpip_init(0, 0);
	lwip_tls_init();

	const static uint8_t mac[] = {0x02, 0x17, 0x78, 0xDA, 0x22, 0x45};
	
	ip_addr_t ip, subnet, gateway, dns;
	
	IP4_ADDR(&ip, 192, 168, 1, 192);
	IP4_ADDR(&subnet, 255, 255, 255, 0);
	IP4_ADDR(&gateway, 192, 168, 1, 1);
	IP4_ADDR(&dns, 192, 168, 1, 1);
	
	static struct netif eth0;
	
	memcpy(eth0.hwaddr, mac, 6);
	eth0.hwaddr_len = 6;
	netif_add(&eth0, &ip, &subnet, &gateway, 0, emac_lwip_init, tcpip_input);
	netif_set_link_down(&eth0);
	netif_set_up(&eth0);
	netif_set_default(&eth0);
	dns_setserver(0, &dns);
//	xTaskCreate(__mbedtls_test, "tls test", 1000, NULL, 2, NULL);
	__mqtt_test_init();
}

static const char * ca_certificate = 
"-----BEGIN CERTIFICATE-----\r\n"
"MIIEkjCCA3qgAwIBAgIQCgFBQgAAAVOFc2oLheynCDANBgkqhkiG9w0BAQsFADA/\r\n"
"MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT\r\n"
"DkRTVCBSb290IENBIFgzMB4XDTE2MDMxNzE2NDA0NloXDTIxMDMxNzE2NDA0Nlow\r\n"
"SjELMAkGA1UEBhMCVVMxFjAUBgNVBAoTDUxldCdzIEVuY3J5cHQxIzAhBgNVBAMT\r\n"
"GkxldCdzIEVuY3J5cHQgQXV0aG9yaXR5IFgzMIIBIjANBgkqhkiG9w0BAQEFAAOC\r\n"
"AQ8AMIIBCgKCAQEAnNMM8FrlLke3cl03g7NoYzDq1zUmGSXhvb418XCSL7e4S0EF\r\n"
"q6meNQhY7LEqxGiHC6PjdeTm86dicbp5gWAf15Gan/PQeGdxyGkOlZHP/uaZ6WA8\r\n"
"SMx+yk13EiSdRxta67nsHjcAHJyse6cF6s5K671B5TaYucv9bTyWaN8jKkKQDIZ0\r\n"
"Z8h/pZq4UmEUEz9l6YKHy9v6Dlb2honzhT+Xhq+w3Brvaw2VFn3EK6BlspkENnWA\r\n"
"a6xK8xuQSXgvopZPKiAlKQTGdMDQMc2PMTiVFrqoM7hD8bEfwzB/onkxEz0tNvjj\r\n"
"/PIzark5McWvxI0NHWQWM6r6hCm21AvA2H3DkwIDAQABo4IBfTCCAXkwEgYDVR0T\r\n"
"AQH/BAgwBgEB/wIBADAOBgNVHQ8BAf8EBAMCAYYwfwYIKwYBBQUHAQEEczBxMDIG\r\n"
"CCsGAQUFBzABhiZodHRwOi8vaXNyZy50cnVzdGlkLm9jc3AuaWRlbnRydXN0LmNv\r\n"
"bTA7BggrBgEFBQcwAoYvaHR0cDovL2FwcHMuaWRlbnRydXN0LmNvbS9yb290cy9k\r\n"
"c3Ryb290Y2F4My5wN2MwHwYDVR0jBBgwFoAUxKexpHsscfrb4UuQdf/EFWCFiRAw\r\n"
"VAYDVR0gBE0wSzAIBgZngQwBAgEwPwYLKwYBBAGC3xMBAQEwMDAuBggrBgEFBQcC\r\n"
"ARYiaHR0cDovL2Nwcy5yb290LXgxLmxldHNlbmNyeXB0Lm9yZzA8BgNVHR8ENTAz\r\n"
"MDGgL6AthitodHRwOi8vY3JsLmlkZW50cnVzdC5jb20vRFNUUk9PVENBWDNDUkwu\r\n"
"Y3JsMB0GA1UdDgQWBBSoSmpjBH3duubRObemRWXv86jsoTANBgkqhkiG9w0BAQsF\r\n"
"AAOCAQEA3TPXEfNjWDjdGBX7CVW+dla5cEilaUcne8IkCJLxWh9KEik3JHRRHGJo\r\n"
"uM2VcGfl96S8TihRzZvoroed6ti6WqEBmtzw3Wodatg+VyOeph4EYpr/1wXKtx8/\r\n"
"wApIvJSwtmVi4MFU5aMqrSDE6ea73Mj2tcMyo5jMd6jmeWUHK8so/joWUoHOUgwu\r\n"
"X4Po1QYz+3dszkDqMp4fklxBwXRsW10KXzPMTZ+sOPAveyxindmjkW8lGy+QsRlG\r\n"
"PfZ+G6Z6h7mjem0Y+iWlkYcV4PIWL1iwBi8saCbGS5jN2p8M+X+Q7UNKEkROb3N6\r\n"
"KOqkqm57TH2H3eDJAkSnh6/DNFu0Qg==\r\n"
"-----END CERTIFICATE-----\r\n";

static void __mbedtls_test(void * pv)
{
	vTaskDelay(2000);
	
	static const char * get_host = "GET https://www.howsmyssl.com/a/check HTTP/1.1\nHost: www.howsmyssl.com\n\n";
	
	TRACE("Current Heap in LAN:%d", xPortGetFreeHeapSize());
	
	tls_configuration_t * conf = lwip_tls_new_conf(TLS_AUTH_SSL_VERIFY_OPTIONAL, ENDNODE_CLIENT);
	
	if(conf)
	{
		TRACE("conf done");
		lwip_tls_add_certificate(conf, (uint8_t *)ca_certificate, strlen(ca_certificate) + 1);

		tls_context_t * ssl = lwip_tls_new(conf, NETCONN_TCP);
		
		if(ssl)
		{
			TRACE("ssl ready");
			
			int32_t ret = lwip_tls_connect(ssl, "www.howsmyssl.com", 443);
			TRACE("connect:%d", ret);
			
			if(ret == 0)
			{
				lwip_tls_write(ssl, (const uint8_t *)get_host, strlen(get_host), 10000);
				
				struct netbuf * nbuf;
				while((ret = lwip_tls_read(ssl, &nbuf, 0)) == 0)
				{
					if(nbuf)
					{
						TRACE("%s", (char *)nbuf->ptr->payload);
						netbuf_delete(nbuf);
						nbuf = 0;
					}
				}
				lwip_tls_delete(ssl);
				lwip_tls_delete_conf(conf);
			}
		}
	}
	TRACE("Current Heap in LAN before suspend:%d", xPortGetFreeHeapSize());
	vTaskSuspend(NULL);
}

#include "mqtt-client.h"

typedef struct {
	uint32_t port;	//0 to disable
	char server[64];
	uint32_t secured;
	char username[64];
	char password[64];
}_S_MQTT_COMMON;

static _S_MQTT_CLIENT_INFO * subs_cinfo = 0;
static TaskHandle_t subs_tsk_handle = 0;

static void __connect_subscribe_failupcall(_S_MQTT_CLIENT_INFO * cinfo) {
	TRACE("mqtt recv process could not re-connect when the connection failed");
	xTaskNotify(subs_tsk_handle, 0, eSetValueWithoutOverwrite);
}

static _S_MQTT_CLIENT_INFO * __client_init(const _S_MQTT_COMMON * s, _S_MQTT_CLIENT_INFO * c, 
																				void (*fail_upcall)(_S_MQTT_CLIENT_INFO * cinfo), tls_configuration_t * conf)
{
	MQTTPacket_connectData * options = 0;
	
	if(c == 0) {
		c = mqtt_client_init(1024, 30000, 30000, fail_upcall);
		if(c) {
			options = mqtt_client_connect_options("agnishant_test", 60, 1, s->username, s->password);
		}
	} else {
		options = c->options;
	}
	while(mqtt_client_is_connected(c) == 0) {
		_E_MQTT_ERRORS err = mqtt_client_connect(c, s->server, s->port, options, conf);
		
		TRACE("mqtt clinet connect, err:%d", err);
		if(err == MQTT_ERROR_NONE) {
			break;
		}
		vTaskDelay(5000);
	}
	return c;
}

uint32_t __subscribe_upcall(_S_MQTT_SUBSCRIBE_INFO * sinfo, uint32_t qos, uint32_t dup, uint32_t retained, 
																		void * payload, uint32_t payload_len)
{
	TRACE("subscribe upcall, qos:%d, dup:%d, retained:%d", qos, dup, retained);
	dump(payload, 0, payload_len);
	
	return 1;
}

static void __mqtt_test(void * pv)
{
	TRACE("Current Heap in LAN:%d", xPortGetFreeHeapSize());
	static const _S_MQTT_COMMON mc = {1883, "mqtt.flespi.io", 0, 0, 0};

	do
	{
		subs_cinfo = __client_init(&mc, subs_cinfo, __connect_subscribe_failupcall, 0);
		
		if(subs_cinfo)
		{
			mqtt_client_subscribe(subs_cinfo, "nishant/subs/test", 2, __subscribe_upcall);
		}
		ulTaskNotifyTake(pdTRUE, portMAX_DELAY);
	} while(1);
}

static void __mqtt_test_init()
{
	xTaskCreate(__mqtt_test, "MQTT TLS", 400, NULL, 2, &subs_tsk_handle);
}

ip_addr_t mqtt_get_interface_ip(ip_addr_t * dest)
{
	return ip_addr_any;
}
